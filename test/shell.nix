{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  buildInputs = [
    pkgs.faust
    pkgs.black
    (pkgs.python3.withPackages (ps: [ ps.scipy ps.pandas ]))
    pkgs.gnuplot
    # keep this line if you use bash
    pkgs.bashInteractive

  ];
}
