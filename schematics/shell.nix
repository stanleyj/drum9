{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  buildInputs = [
    pkgs.geda
    pkgs.ngspice
    # keep this line if you use bash
    pkgs.bashInteractive

  ];
}
