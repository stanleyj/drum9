let pkgs = import <nixpkgs> { };
in pkgs.mkShell {

  buildInputs = [
    pkgs.gnumake
    pkgs.python3
    pkgs.black
    pkgs.faust
    pkgs.faust2alsa
    pkgs.ninja
    # keep this line if you use bash
    pkgs.bashInteractive
  ];
}
