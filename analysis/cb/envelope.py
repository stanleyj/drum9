TUNE_FREQ_LOW = 30e3
TUNE_FREQ_HIGH = 35e3

tune = 1.0

freq = (TUNE_FREQ_HIGH - TUNE_FREQ_LOW) * tune + TUNE_FREQ_LOW

for n in xrange(10,16):
	period = 1.0/(freq/2**n)
	width = 0.5*period
	delay = 0.5*period
	print "V%d (addr%d 0) pulse(iv=0, pv=5, period=%f, width=%f, delay=%f, rise=2n, fall=2n)" % (n, n, period, width, delay)

n=16
period = 1.0/(freq/2**n)
print "V%d (addr%dinv 0) pulse(iv=0, pv=5, period=%f, width=%f, rise=2n, fall=2n)" % (n, n, period, period)



print "V5 (+5v 0) 5"

#audiofreq = 10e3
#print "Vsignal (audiosignal 0) pulse(iv=0, pv=5, period=%f, width=%f, rise=2n, fall=2n)" % ( 1.0/audiofreq, 0.5/audiofreq,  )

print ".list"

print "*.print op iter(0) v(shaper)"
print "*.op"


addr = ' '.join( "v(addr%d)" % n for n in xrange(10,16) )
print ".print tran %s v(addr16inv) v(staircase) v(antilog1) v(antilog2) v(envelope) v(output)" % addr

tsim = 0.5*period+2e-9
print ".transient 0 %f 2.2675736961451248e-05 > envelope.dat" % tsim


