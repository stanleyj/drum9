# this file is needed until upgrade to NixOS 21.05
{ pkgs, buildPythonPackage, ... }:
with pkgs.python3Packages;
let
  asteval = buildPythonPackage rec {
    pname = "asteval";
    version = "0.9.23";

    src = fetchPypi {
      inherit pname version;
      sha256 = "0f54sd4w1a72ij1bcxs2x7dk9xf8bzclawijf1z18bqx9f96l2gm";
    };
    doCheck = false;
  };
in buildPythonPackage rec {
  pname = "lmfit";
  version = "1.0.2";

  src = fetchPypi {
    inherit pname version;
    sha256 = "0iab33jjb60f8kn0k0cqb0vjp1mdskks2n3kpn97zkw5cvjhq2b7";
  };

  propagatedBuildInputs =

    [ numpy uncertainties scipy asteval ];

  doCheck = false;
  #  pythonImportsCheck = [ "PySpice" ];

}
