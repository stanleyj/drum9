import matplotlib.pyplot as plt
import scipy.optimize
import numpy as np
import pickle


class Parameter:
    def __init__(self, name, value=0):
        self.name = name
        self.value = value

    def __float__(self):
        return float(self.value)

    def __str__(self):
        return f"{self.name} = {self.value}"

    def __repr__(self):
        return str((self.name, self.value))

    def __getitem__(self, key):
        if key == 0:
            return self.name
        elif key == 1:
            return self.value
        else:
            raise IndexError("not valid key '{}'".format(key))


class Model:
    def __init__(self, name, f, param_names, initial_values=None):
        self.name = name
        self.function = f
        self.param_names = param_names
        self.initial_values = initial_values

    def parameters(self):
        values = (
            self.fitted_values
            if self.fitted_values is not None
            else self.initial_values
        )
        return [Parameter(n, v) for n, v in zip(self.param_names, values)]

    def fit(self, xs, ys):
        params, cv = scipy.optimize.curve_fit(
            self.function, xs, ys, self.initial_values
        )
        self.fitted_values = params
        return params, cv

    def plot_fitted(self, xs, ys, title=None, show=True):
        plt.plot(xs, ys, ".", label="data")
        plt.plot(xs, self.function(xs, *self.fitted_values), "--", label="fitted")
        title = (
            title
            if title
            else self.name + "\n" + ", ".join(str(p) for p in self.parameters())
        )
        if title:
            plt.title(title)
        if show:
            plt.show()

    def save(self):
        with open(f"{self.name}.model", "wb") as out:
            pickle.dump(self, out)

    @staticmethod
    def load(model_name):
        with open(f"{model_name}.model", "rb") as f_model:
            return pickle.load(f_model)

    def export(self):
        with open(f"{self.name}.par", "w") as out:
            for p in self.parameters():
                print(f"{self.name}_{p}")
                print(f"{self.name}_{p}", file=out)

    def __repr__(self):
        return (
            self.name
            + ": "
            + self.function.__name__
            + "\n\t"
            + "\n\t".join(str(p) for p in self.parameters())
        )


def exp_decay(t, v0, tau, s=0):
    return v0 * np.exp(-t / tau) + s


def linear_max(acc, m, a, b):
    return np.clip(a * acc + b, 0, m)


def simple_20(x_in, a, b, c):
    "Simple_SimpleEquation_20_model"
    return (a + x_in) / (b + c * x_in)


def double_exp(x_in, a, b, c, d, offset):
    "Double Exponential With Offset"
    return a * np.exp(b * x_in) + c * np.exp(d * x_in) + offset


def poly3(a, b, c, d, x):
    return a + b * x + c * np.power(x, 2) + d * np.power(x, 3)


def clipper_tf(x, a, b, c, d, e, f):
    return np.sign(x) * poly3(0, a, b, c, np.abs(x)) / poly3(1, d, e, f, np.abs(x))
