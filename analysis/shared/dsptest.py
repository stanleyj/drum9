import itertools
import subprocess
import shlex
import collections
import os
import re
import numpy as np
from fit import Model


def sweep_range(f,t,step):
    return [x*step for x in range(int(f/step),int(t/step)+1)]


class TestDefinition:
    def __init__(self, name, instrument, process, sweeps={}, num_samples=1000):
        self.name = name
        self.instrument = instrument
        self.executable = "%s_%s_test" % (
            self.instrument,
            re.sub("\W+", "-", self.name),
        )

        self.process = process
        self.num_samples = num_samples
        self.sweeps = sweeps
        self.create_test_dsp()
        self.execute_all()

    def create_test_dsp(self):
        i = self.instrument
        dsp_root = f"../../src/dsp/{i}/"
        dsp_path = f"{dsp_root}/{i}.dsp"
        orig_backup = f"{i}.orig.dsp.bak"
        with open(dsp_path) as dspfile:
            dsptest = (
                line
                for line in dspfile
                if not line.startswith("process") and not line.startswith("declare")
            )
            dsp = "".join(
                itertools.chain(
                    dsptest, ["\n", "process = " + self.process + ";", "\n"]
                )
            )
        os.rename(dsp_path, orig_backup)
        with open(dsp_path, "w") as dsptestfile:
            dsptestfile.write(dsp)
        subprocess.run(shlex.split(f"make {i}_csvplot"), check=True, cwd=dsp_root)
        os.rename(orig_backup, dsp_path)
        os.rename(f"{dsp_root}/{i}_csvplot", self.executable)

    def simulations(self):
        def_sweeps = self.sweeps.copy()
        def_sweeps["n"] = [self.num_samples] # execute each sweep with specified duration
        sweeps = (itertools.product([param],values) for param,values in def_sweeps.items())
        return itertools.product(*sweeps)
    
    def execute_all(self):
        self.results = collections.OrderedDict()
        for params in self.simulations():
            p = dict(params)
            result_key = tuple({k:p[k] for k in self.sweeps.keys()}.items())
            params_arg =  " ".join("-" + x + " " + str(y) for x,y in itertools.chain(params))
            print("executing test", self.name, "with", params_arg)
            self.results[result_key] = np.array(
                [
                    tuple(map(float, value.split(",")))
                    for value in subprocess.run(
                        shlex.split("./%s %s" % (self.executable, params_arg)),
                        universal_newlines=True,
                        stdout=subprocess.PIPE,
                    ).stdout.split("\n")[1:-1]
                ]
            )

    def write_results(self):
        filename = self.executable + ".results"
        with open(filename, "w") as resultsfile:
            for key, values in self.results.items():
                params = dict(key)
                for i, value in enumerate(values):
                    row = "\t".join(
                        str(c)
                        for c in itertools.chain(
                            [i], (params[p] for p in sorted(params)), value,
                        )
                    )
                    resultsfile.write(row + "\n")
                resultsfile.write("\n")
            resultsfile.write("\n")
        print("Results written to", filename)
    
    def __del__(self):
        os.remove(self.executable)


class TestDefinitionTime(TestDefinition):
    def __init__(
        self, name, instrument, process, sweeps={}, duration=1.0, sample_rate=44100
    ):
        num_samples = int(duration * sample_rate)
        if self.process is None:
            self.process = "hastrig(1) : play(%f) : %s" % (duration, process)
        self.sample_rate = sample_rate
        super(TestDefinitionTime, self).__init__(
            name, instrument, self.process, sweeps, num_samples
        )

    def time_axis(self, sweep=()):
        xs = np.arange(self.num_samples) / self.sample_rate
        ys = self.results[sweep]
        return xs, ys

    def plot_model(self, model_file):
        m = Model.load(model_file)
        m.plot_fitted(*self.time_axis())
        return m


class TestDefinitionImpulse(TestDefinitionTime):
    def __init__(
        self, name, instrument, process, sweeps={}, duration=0.5, sample_rate=44100
    ):
        self.process = f"hastrig(1) : {process}"
        super(TestDefinitionImpulse, self).__init__(
            name, instrument, process, sweeps, duration, sample_rate
        )


class TestDefinitionTransfer(TestDefinition):
    def __init__(
        self, name, instrument, process, sweeps={}, inputs=sweep_range(-10, 10, 0.1)
    ):
        num_samples = len(inputs)
        samples = ",".join(str(f) for f in inputs)
        process = "waveform {%s} : (!, _ ) <: (_, %s)" % (samples, process)
        super(TestDefinitionTransfer, self).__init__(
            name, instrument, process, sweeps, num_samples
        )

    def tf_scatter(self, sweep=()):
        return self.results[sweep].T

    def plot_model(self, model_file):
        m = Model.load(model_file)
        m.plot_fitted(*self.tf_scatter())
        return m
