from PySpice.Spice.Parser import SpiceParser
import matplotlib.pyplot as plt
import numpy as np
from fit import Model, exp_decay, linear_max, simple_20

circuit = SpiceParser(path="rs.ckt").build_circuit()
circuit.Vtrig.dc_value = "pulse(0 5 0 2n 2n 2m 1)"

simulations = {}
discharge_start = []
max_charge = 0
for x in range(400, 5001, 200):
    acc = x / 5000.0
    print(f"Simulating Vacc={acc*5}")
    circuit.Vacc.dc_value = acc * 5
    result = circuit.simulator().transient(step_time=4e-5, end_time=0.05)
    env = result["env"]
    ignore_samples = np.argmax(env)
    xs, ys = (
        result.time.as_ndarray()[ignore_samples:],
        env.as_ndarray()[ignore_samples:],
    )
    discharge_start.append(xs[0])
    max_charge = max(max_charge, ys[0])
    xs -= xs[0]
    simulations[acc] = [xs, ys]

decay_start = np.mean(discharge_start)
print("Decay starts at:", decay_start)


capacitor_discharge = Model("exp_decay", exp_decay, ["v0", "tau", "s"])

results = {}
for acc, (xs, ys) in simulations.items():
    ys = ys.copy()
    ys /= max_charge
    capacitor_discharge.initial_values = (ys[0] - ys[-1], 0.048, ys[-1])
    capacitor_discharge.fit(xs, ys)
    capacitor_discharge.plot_fitted(xs, ys, None, False)
    results[acc] = capacitor_discharge.fitted_values

plt.show()


def acc_param(i):
    return np.array([tuple([acc, param[i]]) for acc, param in results.items()]).T


xs, ys = acc_param(0)
fit_v0 = Model("v0", linear_max, ("max", "slope", "offset"), (1, 1, 0))
fit_v0.fit(xs, ys)
fit_v0.plot_fitted(xs, ys)

xs, ys = acc_param(1)
fit_tau = Model("tau", simple_20, ("a", "b", "c"), (0, -1, 21))
fit_tau.fit(xs, ys)
fit_tau.plot_fitted(xs, ys)


xs, ys = acc_param(2)
fit_s = Model("sustain", simple_20, ("a", "b", "c"), (0, 4, 23))
fit_s.fit(xs, ys)
fit_s.plot_fitted(xs, ys)


def envelope(t, acc):
    return exp_decay(
        t,
        linear_max(acc, *fit_v0.fitted_values),
        simple_20(acc, *fit_tau.fitted_values),
        simple_20(acc, *fit_s.fitted_values),
    )


vca_envelope = Model("accent-envelope", envelope, ["accent"])

for acc, (xs, ys) in simulations.items():
    vca_envelope.fitted_values = [acc]
    vca_envelope.plot_fitted(xs, ys / max_charge, show=False)
plt.show()

vca_envelope.save()


with open("accent-envelope.par", "w") as out:
    print(f"accent_envelope_decay_start = {decay_start}", file=out)
    for m in [fit_v0, fit_tau, fit_s]:
        param_name = m.name
        for p, v in m.parameters():
            print(f"accent_envelope_{param_name}_{p} = {v}")
            print(f"accent_envelope_{param_name}_{p} = {v}", file=out)
