from dsptest import TestDefinitionImpulse, sweep_range
from fit import Model
from accent_envelope import vca_envelope

envelope = TestDefinitionImpulse(
    "env_decay",
    "rs",
    "env_decay(ctrl_accent)",
    {"accent": sweep_range(0, 5, 0.5)},
    duration=0.5,
)
