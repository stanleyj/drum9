from PySpice.Spice.Parser import SpiceParser

circuit = SpiceParser(path="rs.ckt").build_circuit()
circuit_without_clipper = SpiceParser(path="rs.ckt").build_circuit()
circuit_without_clipper["D91"].detach()
circuit_without_clipper["D92"].detach()

import pandas as pd


def do_analysis(circuit, outname):
    result = circuit.simulator().transient(step_time=1.0 / 1e6, end_time=0.05)
    return pd.DataFrame({"time": result.time, f"voltage_{outname}": result["shaper"],})


clipper = do_analysis(circuit, "shaped")
no_clipper = do_analysis(circuit_without_clipper, "unshaped")
merged = pd.merge_asof(clipper, no_clipper, on="time")


from fit import Model, clipper_tf


xs, ys = (
    merged.voltage_unshaped,
    merged.voltage_shaped,
)

fit_tf = Model(
    "shaper_oscmix", clipper_tf, ("a", "b", "c", "d", "e", "f"), (1, 0, 3, 0, 1, 5)
)
fit_tf.fit(xs, ys)
fit_tf.plot_fitted(xs, ys)

fit_tf.save()
fit_tf.export()
