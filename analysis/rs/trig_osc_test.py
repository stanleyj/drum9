from dsptest import TestDefinitionImpulse


osctrig = TestDefinitionImpulse(
    "oscillator trigger signal", "rs", "osctrig", duration=0.002
)

m = osctrig.plot_model("osc_trig")
print(m)
