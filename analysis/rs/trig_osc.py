from PySpice.Spice.Parser import SpiceParser

sample_rate = 44100
step_time = 1.0 / sample_rate

circuit = SpiceParser(path="rs.ckt").build_circuit()
trig_analysis = circuit.simulator().transient(step_time=step_time, end_time=0.0015)

import numpy as np
from fit import Model, exp_decay

osctrig = trig_analysis["osctrig"]
ignore_samples = np.argmax(osctrig)
xs, ys = (
    trig_analysis.time.as_ndarray()[ignore_samples:],
    osctrig.as_ndarray()[ignore_samples:],
)


fit_osctrig = Model("osc_trig", exp_decay, ("v0", "tau"), (ys[0], 0.00026))
fit_osctrig.fit(xs, ys)
fit_osctrig.plot_fitted(xs, ys)

fit_osctrig.save()
fit_osctrig.export()


# inspect the parameters
import math

v0, tau = fit_osctrig.fitted_values
print(f"v_trig(t) = {v0} * e^(-t/{tau})")
print(f"tau = {tau * 1e6} µs")
print(f"T60 = {math.log(1000) * tau * 1e6} µs")
