{ pkgs ? import (fetchTarball
  "https://github.com/NixOS/nixpkgs/archive/022caabb5f2265ad4006c1fa5b1ebe69fb0c3faf.tar.gz")
  { }

}:

let
  lmfit = pkgs.callPackage ./lmfit.nix {
    pkgs = pkgs;
    buildPythonPackage = pkgs.python3Packages.buildPythonPackage;
  };
in pkgs.mkShell {
  buildInputs = [
    pkgs.faust
    pkgs.black
    (pkgs.python3.withPackages
      (ps: [ ps.pyspice ps.scipy ps.pandas ps.networkx lmfit ]))
    # keep this line if you use bash
    pkgs.bashInteractive

  ];

  shellHook = ''
    export PYTHONPATH=$PWD/shared:$PYTHONPATH
  '';
}
