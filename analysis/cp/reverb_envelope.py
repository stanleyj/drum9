from PySpice.Spice.Parser import SpiceParser
import matplotlib.pyplot as plt
import numpy as np
from fit import Model, exp_decay, linear_max, simple_20, double_exp
from PySpice.Unit import *

circuit = SpiceParser(path="cp.ckt").build_circuit()

# disable clap envelope generator
circuit["XIC29a"].detach()
circuit["XIC29b"].detach()
circuit["XIC29c"].detach()
circuit["XIC29d"].detach()

in_node = (set(circuit["R213"].nodes) & set(circuit["D52"].nodes)).pop()
circuit.SinusoidalVoltageSource("input", in_node, circuit.gnd, amplitude=15 @ u_V)

circuit["Q36"].detach()
circuit["R213"].detach()
circuit["Vtrig"].detach()


analysis = circuit.simulator().ac(
    start_frequency=1 @ u_nHz,
    stop_frequency=1 @ u_MHz,
    number_of_points=100,
    variation="dec",
)


circuit["Vinput"].detach()
in_node = (set(circuit["C59"].nodes) & set(circuit["D52"].nodes)).pop()
circuit.SinusoidalVoltageSource("input", in_node, circuit.gnd, amplitude=15 @ u_V)
circuit["D52"].detach()
circuit["C59"].detach()

analysis2 = circuit.simulator().ac(
    start_frequency=1 @ u_nHz,
    stop_frequency=1 @ u_MHz,
    number_of_points=100,
    variation="dec",
)


from PySpice.Plot.BodeDiagram import bode_diagram


def bode_plot(analysis, node="env2"):
    figure, axes = plt.subplots(2, figsize=(20, 10))
    bode_diagram(
        axes=axes,
        frequency=analysis.frequency,
        gain=20 * np.log10(np.absolute(analysis[node])),
        phase=np.angle(analysis[node], deg=False),
        marker=".",
        color="blue",
        linestyle="-",
    )

    plt.tight_layout()
    plt.show()


from lmfit import minimize, Parameters, Model


def tf2(s, b2, b1, b0, a1, a0):
    s2 = np.power(s, 2)
    return (b2 * s2 + b1 * s + b0) / (s2 + a1 * s + a0)


gmodel = Model(tf2)  # , prefix="env_reverb_")
x = analysis.frequency.as_ndarray().astype(float)
y = analysis.env2.as_ndarray().astype(complex)

result = gmodel.fit(y, s=x, b2=1, b1=1, b0=1, a1=1, a0=1)

result.plot_fit(ax_kws={"xscale": "log"})
plt.show()
result.plot_fit(ax_kws={"xscale": "log"}, parse_complex="abs")
plt.show()
result.plot_fit(ax_kws={"xscale": "log"}, parse_complex="angle")
