from dsptest import *

INSTRUMENT = "bd"
DURATION = 0.5
DEFAULT_SWEEP = sweep_range(0,1,0.1)

TestDefinitionTime(
    "output", INSTRUMENT,
    "bassdrum",
    {},
    DURATION)

TestDefinitionTime(
    "osc", INSTRUMENT,
    "bdvco",
    { "tune": [0.5]},
    DURATION)

TestDefinitionTime(
    "attackpulse", INSTRUMENT,
    "attackpulse",
    {},
    DURATION)

TestDefinitionTime(
    "env_attack", INSTRUMENT,
    "env_attack(ctrl_accent)",
    { "accent": DEFAULT_SWEEP },
    DURATION)

TestDefinitionTime(
    "env_decay", INSTRUMENT,
    "env_decay(ctrl_accent,ctrl_decay)",
    { "accent": DEFAULT_SWEEP,
      "decay": DEFAULT_SWEEP },
    DURATION)

TestDefinitionTime(
    "env_tune", INSTRUMENT,
    "env_tune(ctrl_tune)",
    { "tune": DEFAULT_SWEEP },
    DURATION)

TestDefinitionImpulse(
    "attackfilter", INSTRUMENT,
    "attackfilter"
    )

TestDefinitionTransfer(
    "shaper", INSTRUMENT,
    "shaper", {},
    sweep_range(-30,30,0.1)
    )
