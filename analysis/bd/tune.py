from PySpice.Spice.Parser import SpiceParser
from PySpice.Unit import kilo, micro
import matplotlib.pyplot as plt
import numpy as np
from fit import Model, exp_decay, simple_20

circuit = SpiceParser(path="bd.ckt").build_circuit()
circuit["XIC13a"].detach()  # disable oscillator

print(circuit)
simulations = {}
discharge_start = []
max_charge = 0
for x in range(0, 101, 10):
    tune = x / 100
    print(f"Simulating tune={tune}")
    circuit.RVR2.resistance = kilo(x)
    result = circuit.simulator().transient(step_time=1e-4, end_time=0.5)
    env = result["env3"]
    ignore_samples = np.argmax(env)
    xs, ys = (
        result.time.as_ndarray()[ignore_samples:],
        env.as_ndarray()[ignore_samples:],
    )
    discharge_start.append(xs[0])
    max_charge = max(max_charge, ys[0])
    xs -= xs[0]
    simulations[tune] = [xs, ys]

decay_start = np.mean(discharge_start)
print("Decay starts at:", decay_start)


capacitor_discharge = Model("exp_decay", exp_decay, ["v0", "tau", "s"])

results = {}
for tune, (xs, ys) in simulations.items():
    ys = ys.copy()
    ys /= max_charge
    capacitor_discharge.initial_values = (ys[0] - ys[-1], 0.048, ys[-1])
    capacitor_discharge.fit(xs, ys)
    capacitor_discharge.plot_fitted(xs, ys, None, False)
    results[tune] = capacitor_discharge.fitted_values

plt.show()


def tune_param(i):
    return np.array([tuple([tune, param[i]]) for tune, param in results.items()]).T


xs, ys = tune_param(0)
fit_v0 = Model("v0", simple_20, ("a", "b", "c"), (0.2, 32, 10))
fit_v0.fit(xs, ys)
fit_v0.plot_fitted(xs, ys)

xs, ys = tune_param(1)
fit_tau = Model("tau", simple_20, ("a", "b", "c"), (0.2, 32, 10))
fit_tau.fit(xs, ys)
fit_tau.plot_fitted(xs, ys)


xs, ys = tune_param(2)
sustain = np.mean(ys)


def envelope(t, tune):
    return exp_decay(
        t,
        simple_20(tune, *fit_v0.fitted_values),
        simple_20(tune, *fit_tau.fitted_values),
        sustain,
    )


vca_envelope = Model("tune-envelope", envelope, ["tune"])

for tune, (xs, ys) in simulations.items():
    vca_envelope.fitted_values = [tune]
    vca_envelope.plot_fitted(xs, ys / max_charge, show=False)
plt.show()

vca_envelope.save()


with open("tune-envelope.par", "w") as out:
    print(f"tune_envelope_decay_start = {decay_start}", file=out)
    print(f"tune_envelope_decay_sustain = {sustain}", file=out)
    for m in [fit_v0, fit_tau]:
        param_name = m.name
        for p, v in m.parameters():
            print(f"tune_envelope_{param_name}_{p} = {v}")
            print(f"tune_envelope_{param_name}_{p} = {v}", file=out)
