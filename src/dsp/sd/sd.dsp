declare name      "Roland TR-909 Snare Drum";
declare version   "1.0";
declare author    "Stanley Jaddoe";
declare license   "GPL";
declare copyright "(c) 2010";
declare options   "[midi:on]";

import("components.lib");
import("envelope-osc1.lib");
import("envelope-osc2.lib");
import("envelope-tone.lib");
import("envelope-tune.lib");
import("sd.lib");
import("shaper-osc1.lib");
import("shaper-osc2.lib");
import("function.lib");

normalize = /(350);

start = ctrl_trig : int : hastrig;

process = start : play(1.5) : snaredrum : normalize;

/*
The snare drum consists of three main parts;
- a noise source, with filters, to form the 'snappy' part
- two triangle wave oscillators, each with its own wave shaper, envelope, 
  and VCA


The snappy part starts with white noise, that is filtered through an RC 
high pass filter, and a Sallen-Key low pass filter.
Then the noise splits in two paths:
1 - tone: the noise is filtered using an RC high pass filter. The amplitude is
          controlled by the tone envelope. The decay of the tone envelope can
          be controlled by the user.
2 - shot: the noise is filtered using a Sallen-Key high pass filter. The 
          amplitude is controlled by an envelope


The frequency of the two triangle wave oscillators is controlled by the tune 
envelope.
This envelope can be controlled by the user. The output of both oscillators is
sent through a wave shaper. Both waves are damped using a decay envelope and a
VCA.
*/

snaredrum(t) = level(snappy(t), vco1(t) + vco2(t));


//--------------------- Decay envelope for Oscillator 1 -----------------------

env_osc1(accent) = generic_envelope(ymax,yinit,tau1,tstart,tdecay,ysustain,tau2)
with
{
	ymax = select3(int(accent<-osc1_ymax_a/osc1_ymax_b) + int(accent<osc1_ymax_bp), 
	               poly1(osc1_ymax_a,osc1_ymax_b,osc1_ymax_bp), 
	               accent : poly1(osc1_ymax_a,osc1_ymax_b), 
	               0 );
	yinit = osc1_yinit;
	tau1 = accent : hyperbexp(osc1_tau1_a,osc1_tau1_b,osc1_tau1_c,osc1_tau1_d);
	tstart = accent : genhyperbolic(osc1_tstart_a,osc1_tstart_b,osc1_tstart_c,osc1_tstart_d);
	tdecay = accent : genhyperbolic(osc1_tdecay_a,osc1_tdecay_b,osc1_tdecay_c,osc1_tdecay_d);
	ysustain = osc1_ysustain;
	tau2 = accent : hyperbexp(osc1_tau2_a,osc1_tau2_b,osc1_tau2_c,osc1_tau2_d);
};


//--------------------- Decay envelope for Oscillator 2 -----------------------

env_osc2(accent) = generic_envelope(ymax,yinit,tau1,tstart,tdecay,ysustain,tau2)
with
{
	ymax = select3(int(accent<-osc2_ymax_a/osc2_ymax_b) + int(accent<osc2_ymax_bp), 
	               poly1(osc2_ymax_a,osc2_ymax_b,osc2_ymax_bp), 
	               accent : poly1(osc2_ymax_a,osc2_ymax_b), 
	               0 );
	yinit = osc2_yinit;
	tau1 = accent : poly1(osc2_tau1_a,osc2_tau1_b);
	tstart = accent : genhyperbolic(osc2_tstart_a,osc2_tstart_b,osc2_tstart_c,osc2_tstart_d);
	tdecay = accent : genhyperbolic(osc2_tdecay_a,osc2_tdecay_b,osc2_tdecay_c,osc2_tdecay_d);
	ysustain = osc2_ysustain;
	tau2 = accent : hyperbexp(osc2_tau2_a,osc2_tau2_b,osc2_tau2_c,osc2_tau2_d);
};

//------------------- VCA using envelope for Oscillator 1 ---------------------

vca_osc1 = *;

//------------------- VCA using envelope for Oscillator 2 ---------------------

vca_osc2 = *;

//--------------- Tune envelope + tune to frequency conversion ----------------

env_tune_systain = poly1(tune_ysustain_a,tune_ysustain_b);

env_tune(tune) = generic_envelope(ymax,yinit,tau1,tstart,tdecay,ysustain,tau2)
with
{
	ymax = poly1(tune_ymax_a,tune_ymax_b,tune);
	yinit = poly1(tune_yinit_a,tune_yinit_b,tune);
	tau1 = tune_tau1;
	tstart = tune_tstart;
	tdecay = poly1(tune_tdecay_a,tune_tdecay_b,tune);
	ysustain = tune : env_tune_systain;
	tau2 = poly2(tune_tau2_a,tune_tau2_b,tune_tau2_c,tune);
};

tune2freq(base_freq_low,base_freq_high,c) = poly1(c,(base_freq-c)/base_env)
with
{
	base_freq = poly1(base_freq_low,base_freq_high-base_freq_low,ctrl_tune);
	base_env = env_tune_systain(ctrl_tune);
};


//----------- Oscillator 1 (frequency controlled by Tune envelope) ------------

tune2freq1 = tune2freq(osc1_freq_base0,osc1_freq_base1,osc1_freq_slope);

shaper1 = generic_waveshaper(shaper_osc1_a,shaper_osc1_b,shaper_osc1_c,shaper_osc1_d,shaper_osc1_e,shaper_osc1_f);

osc1(t) = env_tune(ctrl_tune,t) : tune2freq1 : trianglewaveosc(t==0) * osc1_amplitude;

vco1(t) = env_osc1(ctrl_accent,t), ( osc1(t) : shaper1 ) :> vca_osc1;


//----------- Oscillator 2 (frequency controlled by Tune envelope) ------------

tune2freq2 = tune2freq(osc2_freq_base0,osc2_freq_base1,osc2_freq_slope);

shaper2 = generic_waveshaper(shaper_osc2_a,shaper_osc2_b,shaper_osc2_c,shaper_osc2_d,shaper_osc2_e,shaper_osc2_f);

osc2(t) = env_tune(ctrl_tune,t) : tune2freq2 : trianglewaveosc(t==0) * osc2_amplitude;

vco2(t) = env_osc2(ctrl_accent,t), ( osc2(t) : shaper2 ) :> vca_osc2;


//------------------------------ Tone envelope --------------------------------

env_tone(accent,tone) = generic_envelope(ymax,yinit,tau1,tstart,tdecay,ysustain,tau2)
with
{
	ymax = poly1(tone_ymax_a,tone_ymax_b,min(accent,tone_ymax_bp));
	yinit = tone_yinit;
	tau1 = accent : poly3(tone_tau1_a,tone_tau1_b,tone_tau1_c,tone_tau1_d);
	tstart = accent : poly3(tone_tstart_a,tone_tstart_b,tone_tstart_c,tone_tstart_d);
	tdecay = tone : poly2(tone_tdecay_a,tone_tdecay_b,tone_tdecay_c);
	ysustain = tone_ysustain;
	tau2 = tone : genhyperbolic(tone_tau2_a,tone_tau2_b,tone_tau2_c,tone_tau2_d);

};


//------------------------- VCA using Tone envelope ---------------------------

vca_tone = *;

//--------------------------- Noise shot envelope -----------------------------

env_noiseshot(accent,t) = (int(t>2e-3) * int(t<4e-3))*60e-6*accent; // TODO replace by real envelope


//---------------------------- VCA for noise shot -----------------------------

vca_noiseshot = * ;

//------------------------- Noise filters for snappy --------------------------


filterednoise = whitenoise : voltage_divider(comp_r286,comp_r285) : rchpp(comp_r302,comp_c85) : sklp(comp_r301,comp_r300,comp_c81,comp_c84);

noisesnappy(t) = env_tone(ctrl_accent, ctrl_tone, t), rchpa(comp_r298+comp_r299,-comp_r298,comp_c80) : vca_tone;

noiseshot(t) = env_noiseshot(ctrl_accent,t), skhp(comp_r276,comp_r277,comp_c76,comp_c75) : vca_noiseshot;

snappy(t) = filterednoise <: (noisesnappy(t), noiseshot(t)) :> snappy_lpf(ctrl_snappy);

snappy_lpf(l) = rclpa(1000,r,comp_c77) // TODO: find out input resistance
with {
    r = it.interpolate_linear(ba.lin2LogGain(l), 1, comp_vr9);
};

//------------------------------ Level control --------------------------------

level(snappy,osc) = s+o
with
{
	s = snappy : rchpa(comp_r294, ctrl_level*comp_vr8, comp_c78);
	o = osc*ctrl_level;
};

//------------------------------ User Interface -------------------------------

/* knob layout:

 tune    level
 tone    snappy

*/


ctrl_tune	= hslider("[1]tune [style:knob] [midi:ctrl 106]", 1.0, 0, 1, 0.1);
ctrl_level	= hslider("[2]level [style:knob] [midi:ctrl 107]", 1.0, 0, 1, 0.1);
ctrl_tone	= hslider("[3]tone [style:knob] [midi:ctrl 108]", 1.0, 0, 1, 0.1);
ctrl_snappy	= hslider("[4]snappy [style:knob] [midi:ctrl 109]", 1.0, 0, 1, 0.1);

ctrl_accent	= hslider("[5]accent [midi:key 38]", 1.0, 0, 1, 0.1);
ctrl_trig	= button("[6]snare drum [midi:key 38]");
