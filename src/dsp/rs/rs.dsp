declare name      "Roland TR-909 Rim Shot";
declare version   "1.0";
declare author    "Stanley Jaddoe";
declare license   "GPL";
declare copyright "(c) 2010";
declare options   "[midi:on]";

import("accent-envelope.lib");
import("components.lib");
import("osc-trig.lib");
import("shaper-oscmix.lib");
import("function.lib");

normalize = /(1181);

start = ctrl_trig : int : hastrig;

process = start : rimshot / 100;

/*
The trigger (button press, MIDI note trigger) starts three oscillators, and a
decay envelope.
The outputs of three damped-oscillators are mixed, and then 'shaped' (amplitude
limiting with soft knee).
The output of the wave shaper is sent through a VCA, where the amplitude is 
controlled by the decay envelope.
This output is sent through a variable low-pass filter (effectively, a level 
control) and finally a high-pass filter.
*/

rimshot = _ <: (env_decay(ctrl_accent), oscmix) :> vca_decay : level_lpf( ctrl_level ) : hpf;

//-------------------- Three oscillators + wave shaper ------------------------

shaper = generic_waveshaper(shaper_oscmix_a,shaper_oscmix_b,shaper_oscmix_c,shaper_oscmix_d,shaper_oscmix_e,shaper_oscmix_f);


osctrig = *(osc_trig_v0) : fi.pole(ba.tau2pole(osc_trig_tau));

oscmix = osctrig <: bpf_osc1, bpf_osc2, bpf_osc3 : mixer3 : shaper
with {
    mixer3(s1,s2,s3) = mixer((comp_r408,comp_r415,comp_r417),(s1,s2,s3));
};

//-------------------- Decay envelope (input for VCA) -------------------------

charge_discharge(t_charged,t_discharge,t_empty,minLevel) = keep(t_discharge) : en.arfe(t_charged, t_empty, minLevel);

env_decay(accent) = charge_discharge(accent_envelope_decay_start*0.99,accent_envelope_decay_start,t_fall,sustain) : *(start)
with {
    start = accent : poly1(accent_envelope_v0_offset,accent_envelope_v0_slope) : clamp_max(accent_envelope_v0_max);
    t_fall = accent : simple_20(accent_envelope_tau_a,accent_envelope_tau_b,accent_envelope_tau_c) * 6.91;
    sustain = accent : simple_20(accent_envelope_sustain_a,accent_envelope_sustain_b,accent_envelope_sustain_c);
};

//-------------------- Transistor VCA (transfer function) ---------------------

// the amplitude of the input stream is modified by the control stream env
vca_decay = *;


//----------------- Active Low-pass filter/level control ----------------------


level_lpf(l) = rclpa(comp_r423,r,comp_c120)
with {
    r = it.interpolate_linear(ba.lin2LogGain(l), 1, comp_vr19);
};


//------------------------------- Oscillators ---------------------------------

// Q = 7.3, f = 494 Hz
bpf_osc1 = bridgedt_bpf(comp_r407, comp_r394, comp_c112, comp_c113);

// Q = 6.1, f = 218 Hz
bpf_osc2 = bridgedt_bpf(comp_r414, comp_r411, comp_c115, comp_c116);

// Q = 7.3, f = 1053 Hz
bpf_osc3 = bridgedt_bpf(comp_r416, comp_r404, comp_c117, comp_c118);


//------------------------- Active high-pass filter ---------------------------

// H(s) := a*(s^2 - b*s )/( s^2 + c*s + d );
// analog transfer function is derived from original schematics

hpf = fi.tf2s(b2,b1,b0,a1,a0,1)
with
{
    a = 1 + comp_r421/comp_r419;
    b = comp_r421 * comp_r419 / ( (comp_r421+comp_r419)*comp_r420*comp_r422*comp_c122 );
    c = (1/(comp_r420*comp_c122)) + (1/(comp_r420*comp_c121));
    d = 1/(comp_r420*comp_r422*comp_c121*comp_c122 );
    b2 = a; b1 = -a*b; b0 = 0;
    a1 = c; a0 = d;
};


//------------------------------ User Interface -------------------------------

ctrl_level  = hslider("[1]level [style:knob] [midi:ctrl 14]", 1.0, 0, 1, 0.1);

ctrl_accent     = hslider("[2]accent [midi:key 37]", 1.0, 0, 1, 0.1);
ctrl_trig   = button("[3]rim shot [midi:key 37]");


