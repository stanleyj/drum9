declare name      "Roland TR-909 Crash Cymbal";
declare version   "1.0";
declare author    "Stanley Jaddoe";
declare license   "GPL";
declare copyright "(c) 2010";
declare options   "[midi:on]";

import("cymbal.lib");
import("cbsample.lib");

sample_waveform = cb_waveform;

process = ctrl_trig : cymbal : normalize;


//------------------------------ User Interface -------------------------------

ctrl_tune	= hslider("[1]tune [style:knob] [midi:ctrl 114]", 0.7275, 0, 1, 0.1);
ctrl_level	= hslider("[2]level [style:knob] [midi:ctrl 113]", 1.0, 0, 1, 0.1);

ctrl_accent	= hslider("[2]accent [midi:key 49]", 1.0, 0, 1, 0.1);
ctrl_trig	= int(button("[3]crash cymbal [midi:key 49]")) : ba.impulsify;
