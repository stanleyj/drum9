declare name      "Roland TR-909";
declare version   "1.0";
declare author    "Stanley Jaddoe";
declare license   "GPL";
declare copyright "(c) 2010";

mono2stereo = _  <: _,_ ;
// NOTE: .dsp.exp are generated using `faust -e` by the build system
bd = component("bd/bd.dsp.exp") : mono2stereo;
sd = component("sd/sd.dsp.exp") : mono2stereo;
lt = component("toms/lt.dsp.exp") : mono2stereo;
rs = component("rs/rs.dsp.exp") : mono2stereo;
cp = component("cp/cp.dsp.exp") : mono2stereo;
hh = component("hh/hh.dsp.exp") : mono2stereo;
cb = component("cymbals/cb.dsp.exp") : mono2stereo;
rb = component("cymbals/rb.dsp.exp") : mono2stereo;

process = hgroup("TR-909",
		vgroup("[01]bass drum", bd),
		vgroup("[02]snare drum", sd),
		vgroup("[03]low tom", lt),
		vgroup("[06]rim shot", rs),
		vgroup("[07]hand clap", cp),
		vgroup("[08]hi-hat", hh),
		vgroup("[09]crash cymbal", cb),
		vgroup("[10]ride cymbal", rb)
		) :> _,_;

