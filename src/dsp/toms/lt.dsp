declare name      "Roland TR-909 Low Tom";
declare version   "1.0";
declare author    "Stanley Jaddoe";
declare license   "GPL";
declare copyright "(c) 2010";
declare options   "[midi:on]";

import("lt.lib");
import("toms.lib");

process =  ctrl_trig : framecount(dt) : tom : normalize;


//------------------------------ User Interface -------------------------------

/* knob layout:

 tune    level
 decay

*/


ctrl_tune	= hslider("[1]tune [style:knob] [midi:ctrl 20]", 1.0, 0, 1, 0.1);
ctrl_level	= hslider("[2]level [style:knob] [midi:ctrl 21]", 1.0, 0, 1, 0.1);
ctrl_decay	= hslider("[3]decay [style:knob] [midi:ctrl 22]", 1.0, 0, 1, 0.1);

ctrl_accent	= hslider("[5]accent [midi:key 41]", 1.0, 0, 1, 0.1);
ctrl_trig	= int(button("[6]low tom [midi:key 41]"));
