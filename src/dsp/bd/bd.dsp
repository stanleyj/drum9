declare name      "Roland TR-909 Bass Drum";
declare version   "1.0";
declare author    "Stanley Jaddoe";
declare license   "GPL";
declare copyright "(c) 2010";
declare options   "[midi:on]";

import("attack-filter.lib");
import("attack-pulse.lib");
import("bd.lib");
import("components.lib");
import("envelope-attack.lib");
import("envelope-decay.lib");
import("envelope-tune.lib");
import("shaper-osc.lib");
import("function.lib");

normalize = rclpp(1000,0.01e-6) : /(145);

start = ctrl_trig : int : hastrig;

process = start : play(1.5) : bassdrum : normalize;

/*
The trigger (button press, MIDI note trigger) starts a triangle wave 
oscillator, a decay envelope, and an 'attack' pulse.

The frequency of the triangle wave oscillator is controlled by the tune 
envelope.  The triangle wave is shaped, with a 'sine' wave as result.
The output of the wave shaper is sent through a VCA, where the amplitude is 
controlled by the decay envelope.

The attack part, consists of a single pulse wave, and bandpass-filtered noise. 
The shape of the attack sound is controlled by the attack envelope.

Both sounds are mixed, and the volume of the output is controlled by the level
control parameter.
*/


bassdrum(t) = env_decay(ctrl_accent, ctrl_decay, t), bdvco(t) :> vca_decay + attack(t)  : *(ctrl_level);



//------------------------------ Attack pulse ---------------------------------

attackpulse_calc(t) = select2(t<t_neg2pos, pos, neg)
with
{
	neg = -generic_envelope(pulse_neg_ymax,pulse_neg_yinit,pulse_neg_tau1,pulse_neg_tstart,pulse_neg_tdecay,pulse_neg_ysustain,pulse_neg_tau2,t);
	pos =  generic_envelope(pulse_pos_ymax,pulse_pos_yinit,pulse_pos_tau1,pulse_pos_tstart,pulse_pos_tdecay,pulse_pos_ysustain,pulse_pos_tau2,t);
};


attackpulse = sampledsignal( attackpulse_calc ); // signal cannot be influenced by user-parameters, so we sample it for performance reasons

//------------------------- Attack bandpass filter ----------------------------

attacknoise = whitenoise : voltage_divider(comp_r10,comp_r9) : attackfilter;

attackfilter = passive_bpf(r1,r2,r3,c1,c2)
with {
    c1 = comp_c15;
    c2 = comp_c13;
    r1 = comp_r46;
    r2 = comp_r45;
    r3 = comp_r56;
};




//---------------------------- Attack envelope --------------------------------

env_attack(accent) = generic_envelope(ymax,yinit,tau1,tstart,tdecay,ysustain,tau2)
with
{
	ymax = max(max(yinit,ysustain),
                   poly1(attack_ymax_a,attack_ymax_b,min(accent,attack_ymax_bp)));
	yinit = accent : poly1(attack_yinit_a,attack_yinit_b);
	tau1 = max(0, 
	           accent : genhyperbolic(attack_tau1_a,attack_tau1_b,attack_tau1_c,attack_tau1_d) );
	tstart = accent : genhyperbolic(attack_tstart_a,attack_tstart_b,attack_tstart_c,attack_tstart_d);
	tdecay = accent : poly3(attack_tdecay_a,attack_tdecay_b,attack_tdecay_c,attack_tdecay_d);
	ysustain = attack_ysustain;
	tau2 = accent : genhyperbolic(attack_tau2_a,attack_tau2_b,attack_tau2_c,attack_tau2_d);
};


//-------------------------- Attack Transistor VCA ----------------------------

vca_attack = *;


//------------------------- Attack (noise+pulse) ------------------------------

noisepulse(t) = attackpulse(t), attacknoise :> voltage_divider(vr3,comp_r55)
with {
    vr3 = it.interpolate_linear(ba.lin2LogGain(ctrl_attack), comp_vr3, 1);
};


attack = _ <: env_attack(ctrl_accent), noisepulse :> vca_attack;


//------------------------------ Decay envelope -------------------------------

env_decay(accent,decay) = generic_envelope(ymax,yinit,tau1,tstart,tdecay,ysustain,tau2)
with
{
	decay_ymax_a = decay : invlinear(decay_ymax_a_a,decay_ymax_a_b,decay_ymax_a_c);
	decay_ymax_b = decay : invquadratic(decay_ymax_b_a,decay_ymax_b_b,decay_ymax_b_c,decay_ymax_b_d);
	ymax = max(max(yinit,ysustain),
	           poly1(decay_ymax_a,decay_ymax_b,min(accent,decay_ymax_bp)));
	yinit = decay : poly1(decay_yinit_a,decay_yinit_b);
	tau1 = accent : invquadratic(decay_tau1_a,decay_tau1_b,decay_tau1_c,decay_tau1_d);
	tstart = decay_tstart;
	tdecay = accent : poly2(decay_tdecay_a,decay_tdecay_b,decay_tdecay_c);
	ysustain = decay_ysustain;
	tau2 = decay : hyperbexp(decay_tau2_a,decay_tau2_b,decay_tau2_c,decay_tau2_d);
};

//-------------------------- Decay Transistor VCA -----------------------------

vca_decay = *;

//------------------------------ Tune envelope --------------------------------

env_tune(tune) = generic_envelope(ymax,yinit,tau1,tstart,tdecay,ysustain,tau2)
with
{
	ymax = tune : hyperbexp(tune_ymax_a,tune_ymax_b,tune_ymax_c,tune_ymax_d);
	yinit = tune_yinit;
	tau1 = tune : hyperbexp(tune_tau1_a,tune_tau1_b,tune_tau1_c,tune_tau1_d);
	tstart = tune : poly3(tune_tstart_a,tune_tstart_b,tune_tstart_c,tune_tstart_d);
	tdecay = tune_tdecay;
	ysustain = tune_ysustain;
	tau2 = tune : hyperbexp(tune_tau2_a,tune_tau2_b,tune_tau2_c,tune_tau2_d);
};



//------------ Oscillator (frequency controlled by Tune envelope) -------------

tune2freq = poly1(tune_base_freq,tune_slope_freq);

shaper = generic_waveshaper(shaper_osc_a,shaper_osc_b,shaper_osc_c,shaper_osc_d,shaper_osc_e,shaper_osc_f);

bdvco(t) = env_tune(ctrl_tune,t) : tune2freq : trianglewaveosc(t==0) * triangle_wave_amplitude : shaper;


//------------------------------ User Interface -------------------------------

/* knob layout:

 tune    level
 attack  decay

*/

ctrl_tune	= hslider("[1]tune [style:knob] [midi:ctrl 102]", 1.0, 0, 1, 0.1);
ctrl_level	= hslider("[2]level [style:knob] [midi:ctrl 103]", 1.0, 0, 1, 0.1);
ctrl_attack	= hslider("[3]attack [style:knob] [midi:ctrl 104]", 1.0, 0, 1, 0.1);
ctrl_decay	= hslider("[4]decay [style:knob] [midi:ctrl 105]", 1.0, 0, 1, 0.1);

ctrl_accent	= hslider("[5]accent [midi:key 36]", 1.0, 0, 1, 0.1);
ctrl_trig	= button("[6]bass drum [midi:key 36]");
