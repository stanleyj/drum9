declare name      "Roland TR-909 Hi-Hats";
declare version   "1.0";
declare author    "Stanley Jaddoe";
declare license   "GPL";
declare copyright "(c) 2010";
declare options   "[midi:on]";

import("components.lib");
import("envelope-decay-chh.lib");
import("envelope-decay-ohh.lib");
import("envelope-decay.lib");
import("tune.lib");
import("hhsample.lib");
import("function.lib");

normalize = /(123);

//process = hh_trig : framecount : ba.samp2sec : hihat : normalize;
process = ctrl_trig_ohh, ctrl_trig_chh : hihat : normalize;


//-------------------------- Hi-Hat Sampler ROM -------------------------------


sampler_index(trig) = play_waveform_index(hh_waveform,trig);

read_sample(o,c,i) = address : sample(hh_waveform) : >>(2)
with
{
	chh_start = 2^15 - 2^13;
	ctrl_trig_ohh_stream = srflipflop(o,c);
	chh = i+chh_start;
	ohh = min(i, chh_start-1);
	address = int(select2( ctrl_trig_ohh_stream, chh, ohh ));
};

//-------------------------- Decay envelope OHH -------------------------------

env_decay_ohh(accent,decay) = generic_envelope(ymax,yinit,tau1,tstart,tdecay,ysustain,tau2)
with
{
	ymax =  decayohh_ymax_a / (1.0 + exp(poly1(decayohh_ymax_b,-decayohh_ymax_c,accent)));
	yinit = decayohh_yinit;
	tau1 = accent : poly4(decayohh_tau1_a,decayohh_tau1_b,decayohh_tau1_c,decayohh_tau1_d,decayohh_tau1_e);
	tstart = accent : poly1(decayohh_tstart_a,decayohh_tstart_b);
	tdecay = decayohh_tdecay;
	ysustain = decayohh_ysustain;
	tau2 = poly1(decayohh_tau2_a,decayohh_tau2_b,decay) / (decayohh_tau2_c + decay);
};

//-------------------------- Decay envelope CHH -------------------------------

env_decay_chh(accent,decay) = generic_envelope(ymax,yinit,tau1,tstart,tdecay,ysustain,tau2)
with
{
	ymax =  decaychh_ymax_a / (1.0 + exp(poly1(decaychh_ymax_b,-decaychh_ymax_c,accent)));
	yinit = decaychh_yinit;
	tau1 = accent : poly4(decaychh_tau1_a,decaychh_tau1_b,decaychh_tau1_c,decaychh_tau1_d,decaychh_tau1_e);
	tstart = accent : poly2(decaychh_tstart_a,decaychh_tstart_b,decaychh_tstart_c);
	tdecay = decaychh_tdecay;
	ysustain = decaychh_ysustain;
	tau2 = poly1(decaychh_tau2_a,decaychh_tau2_b,decay) / (decaychh_tau2_c + decay);
};

//-------------------------- Decay envelope selector -------------------------------


env_decay(accent,o,c) = select2( ctrl_trig_ohh_stream, env_chh, env_ohh )
with
{
	t = framecount(dt,o|c);
	ctrl_trig_ohh_stream = srflipflop(o,c);
	env_chh = env_decay_chh(accent,ctrl_decay_chh,t);
	env_ohh = env_decay_ohh(accent,ctrl_decay_ohh,t);
};

//-------------------------------- Filters ------------------------------------

hpfilter_dac = rchpp(comp_r431,comp_c127);
hpfilter_level = rchpa(comp_r445,r,comp_c132)
with {
    r = it.interpolate_linear(ctrl_level, 1, comp_vr24);
};

lp1 = sklp(comp_r456,comp_r457,comp_c136,comp_c137);
lp2 = sklp(comp_r459,comp_r461,comp_c138,comp_c139);



//----------------------------- Transistor VCA --------------------------------


vca_decay(env,x) = env*x;/*1.0 / ( c1 * exp(c2*x) + c3 * exp(c4*x)) + c5
with
{
	c1 = vca_decay_c1_a*env / (1.0 + ((env-vca_decay_c1_b)/vca_decay_c1_c)^vca_decay_c1_d);
	c2 = vca_decay_c2_a+vca_decay_c2_b/env+vca_decay_c2_c*env;
	c3 = 1.0 / ( vca_decay_c3_a * exp(-0.5 * (log(env/vca_decay_c3_b)/vca_decay_c3_c)^vca_decay_c3_d));
	c4 = (vca_decay_c4_a*env^2 + vca_decay_c4_b*log(env) ) / (1.0 + vca_decay_c4_c*env + vca_decay_c4_d*log(env) ) + vca_decay_c4_e;
	c5 = env : poly2(vca_decay_c5_a,vca_decay_c5_b,vca_decay_c5_c);
};
*/



hihat(o,c) = env_decay(ctrl_accent,o,c), ( rom_read_freq : sampler_index(o|c) : read_sample(o,c) : signal_dac6 : hpfilter_dac ) : vca_decay : lp1 : lp2 : hpfilter_level;


//------------------------------ User Interface -------------------------------

ctrl_decay_chh	= hslider("[1]decay closed hi-hat [style:knob] [midi:ctrl 111]", 1, 0, 1, 0.1);
ctrl_decay_ohh	= hslider("[1]decay open hi-hat [style:knob] [midi:ctrl 112]", 1, 0, 1, 0.1);
ctrl_level	= hslider("[2]level [style:knob] [midi:ctrl 110]", 1.0, 0, 1, 0.1);

ctrl_accent	= hslider("[2]accent [midi:key 42]", 1.0, 0, 1, 0.1);
ctrl_trig_ohh	= int(button("[3]open hi-hat [midi:key 46]")) : ba.impulsify;
ctrl_trig_chh	= int(button("[4]closed hi-hat [midi:key 42]")) : ba.impulsify;
