declare name      "Roland TR-909 Hand Clap";
declare version   "1.0";
declare author    "Stanley Jaddoe";
declare license   "GPL";
declare copyright "(c) 2010";
declare options   "[midi:on]";

import("components.lib");
import("function.lib");

normalize = *(7);

process = ctrl_trig : hastrig : handclap : normalize;

/*
The trigger signal starts two envelope generators:
- envelope for reverb effect
- the hand clap envelope

The envelope for the reverb effect is basically a second-order low-pass filter
of the trigger signal, while the hand clap envelope starts a pulse generator for
a limited amount of time, generating three pulses with a short decay time.

When the time for the pulse generator has passed, a fourth pulse is generated,
with a longer decay time.

The envelopes are used to shape noise from the white noise generator. The white
noise is first filtered using a band pass filter, then split in two parts with
their own additional filter stages: the reverb part and the clap part.

At the end, the reverb and the clap signals are mixed together to form the hand
clap sound.

*/


handclap = _,ctrl_accent,noise_base <: env_clap,noise_clap,env_reverb,noise_reverb :*,* :> _,ctrl_level : *
with {
    noise_base = whitenoise : multi_feedback_bp(comp_r208,comp_r209,comp_r207,comp_c42c43);
};


//---------------------------------- Reverb -----------------------------------

noise_reverb = passive_bpf(r1,r2,r3,c1,c2)
with {
    r1 = comp_r204;
    r2 = comp_r205;
    r3 = comp_r203;
    c1 = comp_c55;
    c2 = comp_c57;
};

env_reverb(accent) = keep(0.002)*13.5*accent : rclpp(500000, comp_c59) : rclpp(comp_r233,comp_c61) : *(100);


//---------------------------------- Clap -------------------------------------

noise_clap = rchpa(comp_r201+comp_r202,-comp_r201,comp_c56);

decay(t) = en.are(1/ma.SR,t);

env_clap(trig,accent) = trig <: claps_initial_duration,_ <: _,!,clap_last,!,claps_initial : select2 : *(accent)
with {
    pulsegen = os.lf_sawpos_phase(79) <: <=(_');
    claps_initial_duration = keep(0.031);
    claps_initial(enabled) = pulsegen : *(enabled) : decay(0.03);
    clap_last_start(d) = (d==0) & (d'==1);
    clap_last = clap_last_start : decay(0.15);
};


//------------------------------ User Interface -------------------------------

ctrl_level	= hslider("[1]level [style:knob] [midi:ctrl 15]", 1.0, 0, 1, 0.1);

ctrl_accent	= hslider("[2]accent [midi:key 39]", 1.0, 0, 1, 0.1);
ctrl_trig	= button("[3]hand clap [midi:key 39]");
